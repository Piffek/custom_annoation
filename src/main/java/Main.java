import annotations.NewField;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import entities.Post;
import serialize.JsonSerializableField;
import serialize.JsonSerializer;

public class Main {
    public static void main(String[] args) {
        try {
           Post post = new Post();
           post.setBody("cos");
           post.setTitle("title");

            JsonSerializer jsonSerializer = new JsonSerializer(new JsonSerializableField());
            System.out.println(jsonSerializer.serialize(post));

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
