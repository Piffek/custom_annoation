package serialize;

import annotations.NewField;
import entities.Post;
import static java.util.Objects.requireNonNull;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class JsonSerializableField {
    public List<SerializeField> getSerializableField(Object obj) throws SerializeException {
        try {
            Class<?> objClass = requireNonNull(obj).getClass();
            List<SerializeField> fields = new ArrayList<>();

            for(Field field : objClass.getDeclaredFields()) {
                field.setAccessible(true);

                if(field.isAnnotationPresent(NewField.class)) {
                    SerializeField serializeField = new SerializeField(getSerializableKey(field), (String) field.get(obj));
                    fields.add(serializeField);
                }
            }
            return fields;
        }catch (IllegalAccessException e) {
            throw new SerializeException(e.getMessage());
        }
    }

    private static String getSerializableKey(Field field) {
        String annotationValue = field.getAnnotation(NewField.class).value();

        if (!annotationValue.isEmpty()) {
            return annotationValue;
        }

        return field.getName();
    }
}
