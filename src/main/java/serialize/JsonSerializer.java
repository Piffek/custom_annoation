package serialize;

import entities.Post;
import java.util.List;
import static java.util.stream.Collectors.joining;

public class JsonSerializer {
    private JsonSerializableField jsonSerializableField;

    public JsonSerializer(JsonSerializableField field) {
        this.jsonSerializableField = field;
    }

    public String serialize(Object obj) throws SerializeException {
        List<SerializeField> setField = jsonSerializableField.getSerializableField(obj);
        return toJsonString(setField);
    }

    private String toJsonString(List<SerializeField> fields) {
        String elementsString = fields
                .stream()
                .map(field -> "\""  + field.getName() + "\":\"" + field.getValue() + "\"")
                .collect(joining(", "));
        return "{" + elementsString + "}";
    }
}
