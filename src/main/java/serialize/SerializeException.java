package serialize;

public class SerializeException extends Exception {
    SerializeException(String message) {
        super(message);
    }
}
