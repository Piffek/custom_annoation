package serialize;

public class SerializeField {

    private String value;
    private String name;

    public SerializeField(String value, String name) {
        this.name = name;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
